﻿USE provincias;
/* Hoja de Ejemplos 1 */

/* Ej1v1 - IF */
DROP PROCEDURE IF EXISTS ej1v1;
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej1v1 (a int, b int)
    BEGIN
      IF (a > b) THEN
        SELECT a;
      ELSE
        SELECT b;
      END IF;
    END //
DELIMITER ;

CALL ej1v1(12,5);

/* Ej1v2 - TEMPORARY TABLE */
DROP PROCEDURE IF EXISTS ej1v2;
DELIMITER //
  CREATE PROCEDURE IF NOT EXISTS ej1v2(a int, b int)
    BEGIN
      CREATE OR REPLACE TEMPORARY TABLE  numeros(
        numero int
        );
      INSERT INTO numeros (numero) VALUE (a),(b);
      SELECT MAX(numero) FROM numeros;
    END //
DELIMITER ;

CALL ej1v2(12,5);

/* Ej1v3 - GREATEST */
DROP PROCEDURE IF EXISTS ej1v3;
DELIMITER //
  CREATE PROCEDURE IF NOT EXISTS ej1v3(a int, b int)
    BEGIN
      SELECT GREATEST(a,b);
    END //
DELIMITER ;

CALL ej1v3(5,9);


/* Ej2v1 - IF */
DROP PROCEDURE IF EXISTS ej2v1;
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej2v1(a int, b int, c int)
    BEGIN
      IF (a>b) THEN
        IF (a>c) THEN
           SELECT a;
        ELSE
           SELECT c;
        END IF;
      ELSE
        IF (c>b) THEN
          SELECT c;
        ELSE
          SELECT b;
        END IF;
      END IF;
    END //
DELIMITER ;

CALL ej2v1(5,7,2);

/* Ej2v2 - TEMPORARY TABLE */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej2v1(a int, b int, c int)
    BEGIN
      CREATE OR REPLACE TEMPORARY TABLE numeros (
          numero int     
        );
      INSERT INTO numeros VALUE (a),(b),(c);
      
      SELECT MAX(numero) FROM numeros ;

    END //
DELIMITER ;

CALL ej2v1(10 , 8 ,2);

/* Ej2v3 - GREATEST */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej2v3(a int, b int, c int)
    BEGIN
      SELECT GREATEST(a,b,c);
    END //
DELIMITER ;

CALL ej2v3(34,23,51);

/* Ej3 */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej3(a int, b int, c int, OUT v1 int, OUT v2 int)
    BEGIN
      SET v1 = GREATEST(a,b,c);
      SET v2 = LEAST(a,b,c);
    END //
DELIMITER ;

CALL ej3 (11, 22, 33 , @grande, @pequeno);
SELECT @grande,@pequeno;

/* Ej4 */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej4(f1 date,f2 date)
    BEGIN
      DECLARE resultado int DEFAULT 0;

      IF (f1>f2) THEN
        SET resultado = DATEDIFF(f1,f2);
      ELSE
        SET resultado = DATEDIFF(f2,f1);
      END IF;      
  
      SELECT resultado;

    END //
DELIMITER ;

CALL ej4('2010/1/1','2010/1/10');

/* Ej5 */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej5(f1 date, f2 date)
    BEGIN
      DECLARE resultado int DEFAULT 0;

      IF (f1>f2) THEN
        SET resultado = TIMESTAMPDIFF(MONTH,f1,f2);
      ELSE
        SET resultado = TIMESTAMPDIFF(MONTH,f2,f1);
      END IF;      
  
      SELECT resultado;
    END //
DELIMITER ;

CALL ej5('2010/1/1','2010/1/10');


/* Ej6 */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej6(f1 date, f2 date,OUT dias int,OUT meses int,OUT anios int)
    BEGIN
      SET dias = TIMESTAMPDIFF(DAY, f2, f1);
      SET meses = TIMESTAMPDIFF(MONTH, f2, f1);
      SET anios = TIMESTAMPDIFF(YEAR ,f2, f1);
             
      IF (f1>f2) THEN
        SET dias = TIMESTAMPDIFF(DAY, f1, f2);
        SET meses = TIMESTAMPDIFF(MONTH, f1, f2);
        SET anios = TIMESTAMPDIFF(YEAR ,f1, f2); 
      END IF;      
  
    END //
DELIMITER ;

CALL ej6 ('2010/1/1','2020/1/10',@d,@m,@y);
SELECT @d,@m,@y;

/* Ej7 */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej7(texto varchar(50))
    BEGIN
      DECLARE longitud int DEFAULT 0;
      SET longitud = CHAR_LENGTH(texto);
      SELECT longitud;  
    END //
DELIMITER ;

CALL ej7('Este es una prueba');
